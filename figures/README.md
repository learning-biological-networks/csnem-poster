# Figures

Figures contained in this folder:

* `UWlogo_ctr_4c.pdf`, an oficial UW logo, obtained from [University Marketing](https://umark.wisc.edu/brand/templates-and-downloads/print-logos.php), other versions are available there.
* `NIH_NLM_BLU_4-white.png`, an official NIH-NLM logo, obtained from [the NLM website](https://www.nlm.nih.gov/about/logos_and_images.html).
* `nlm_logo.eps`, a vector version of the NIH-NLM logo
* `csnem.pdf`, the CSNEM diagram
* `nem.pdf`, the NEM diagram